shadows
=======

Tagged key-value store: Set up metadata precedence rules. Automatically load the right data for the current context.
